# Webbeheer Laravel (deprecated)

Connector for Webbeheer structure and Laravel.

## Installation

Install the package.
 
Add ``WebbeheerLaravel\Providers\StructureServiceProvider::class,`` to your serviceproviders in `config/app.php`.

Add controllers

## Usage



## Example
 
Add a controller to `app/Http/Controllers/Portlets` implementing the `PortletControllerInterface`. This controller will show in the structure module. The public methods will be the available views for that portlet. 
 
### Controller

```php
<?php

namespace App\Http\Controllers\Portlets;

use App\Models\Tekstblok;
use WebbeheerLaravel\Contracts\PortletControllerInterface;
use WebbeheerLaravel\Structure\Model\Contents;

class TekstblokController extends \App\Http\Controllers\Controller implements PortletControllerInterface {

    public function tekst(Contents $contents) {

        $collection = $contents->parameters()->where('parameter', '=', 'tekstblok_id');
        if($collection->count() !== 0){
            return view('tekstblok/tekst', ['tekstblok' => Tekstblok::find($collection->first()->waarde)]);
        }
    }

    public static function getHiddenActions(){
        return [];
    }
}
```


### Layout

A layout is a collection of zones where you can add portlets to. A basic layout is supplied by the package.
 
```html
<!DOCTYPE html>
<!--[if lte IE 8]> <html class="no-js ie8" lang="nl"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="nl"> <!--<![endif]-->
<head>
    {!! @$headHtml !!}
</head>
<body id="layout-standaard">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ config('ROOTURL') }}">{{ config('KLANTNAAM') }}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            {!! @$Mainnav !!}
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
            {!! @$leftColumn !!}
        </div>
        <div class="col-xs-12 col-md-6">
            {!! @$middleColumn !!}
        </div>
        <div class="col-xs-12 col-md-3">
            {!! @$rightColumn !!}
        </div>
    </div>
</div>


<footer id="footer" class="container">
    <div class="row">
        <div class="col-xs-12">
            {!! @$Footernav !!}
        </div>
    </div>

</footer>

{!! @$headHtmlBottom !!}
</body>
</html>
```

## Database Seeding

`WebbeheerLaravel\Database\Seeding\Seeder` is a convenient helper class to seed Webbeheer users, user groups, permissions, modules and module categories.

### Example seeder

```php
<?php

use Illuminate\Database\Seeder;
use WebbeheerLaravel\Database\Seeding\Seeder as WebbeheerSeeder;

class WebbeheerDatabaseSeeder extends Seeder
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    public function __construct()
    {
        $this->seeder = new WebbeheerSeeder();
    }

    public function run()
    {
        $this->seeder->addUserGroup('Administrators');

        $this->seeder->addUser()
            ->setGroup('Administrators')
            ->setUsername('foo')
            ->setEmailaddress('foobar@swis.nl')
            ->setInitials('F.O.O.')
            ->setLastName('Bar');

        $this->seeder->addModuleCategory(
            'Content',
            'Lorem ipsum.'
        );

        $this->seeder->addModule('labels', 'Labels', 'Content')
            ->withUserGroupPermissions(['Administrators']);

        // N.B. Truncating is optional. When you don't truncate, all existing records will be updated, except users.
        // This allows you to easily add modules or users while keeping the existing records.
        $this->seeder->truncateAll();
        $this->seeder->createAll();
    }
}
```
