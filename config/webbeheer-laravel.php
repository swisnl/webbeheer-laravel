<?php
return [

    /**
     * Base controller route to manage the layout
     */
    'layout_controller_route' => 'WebbeheerLaravel\Http\LayoutController@buildLayout',

    /**
     * Default namespace for resolved Webbeheer module controllers.
     */
    'module_controller_namespace' => 'App\Http\Controllers\Portlets',

    /**
     * Path to module controllers.
     */
    'module_controller_path' => 'app/Http/Controllers/Portlets',

    /**
     * View composer for the headHtml should you want to overwrite this.
     */
    'headhtml_view_composer' => 'WebbeheerLaravel\ViewComposers\HeadHtmlComposer',

    /**
     * Content controller and action name resolver.
     */
    'content_controller_resolver' => 'WebbeheerLaravel\Structure\Resolvers\ContentControllerResolver',

    /**
     * Generator for generating the routes based on the tree.
     * Must be an instance of WebbeheerLaravel\Contracts\NodeRoutesGeneratorContract
     */
    'node_routes_generator' => 'WebbeheerLaravel\Structure\Routing\NodeRoutesGenerator',

    /**
     * Default middleware for structure routes.
     */
    'node_routes_middleware' => ['web'],

    /**
     * TTL for eloquent cache in minutes
     */
    'structure_eloquent_model_cache_ttl' => env('SWIS_LARAVEL_STRUCTURE_ELOQUENT_TTL', 0.25),

    /**
     * Relative path to the layouts in the views folder
     */
    'view_layouts_folder' => 'layouts',

    /**
     * Some included controllers in the package
     */

    /**
     * Navigation
     */
    'navigation_controller_class' => 'WebbeheerLaravel\Http\NavigationController',
    
];
