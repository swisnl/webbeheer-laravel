# 2.10.0

* Laravel 7 and 8 compatibility.

# 2.9.0

* Laravel 6 compatibility.

# 2.8.0

* Upgrade watson/rememberable to 3.x.

# 2.7.0

* Laravel 5.7 and 5.8 compatibility.

# 2.6.0

* Laravel 5.6 compatibility.


# 2.5.0

* Improve DatabaseSeeder. Added possibility to update existing records. See readme for example usage.

# 2.4.0

* Add DatabaseSeeder. See readme for example usage.

# 2.3.0

* Set meta description based on `Node->description` property.

# 2.2.0

* Prevent hidden content from beeing displayed.
* Make NodeRoutesGenerator easily extendable.

# 2.1.5 

* If there is content that isn't placed on any node return null instead of triggering property of none object.

# 2.1.4

* Add laravel 5.5 to composer.json

# 2.1.3

* Ignore constructors and other internal methods when enumerating possible views

# 2.1.1

* Get path to node refers to default node if no name is set. 

# 2.1.0

* Handle default routes for links to nodes when no custom route is given.
* Fix checking if route_name is empty instead of not set.

# 2.0.2

* Bugfix in navigation generation 

# 2.0.1 

* Add web middleware by default, since installs use 5.3 and 5.4 anyways.

# 2.0.0

* Cleanup of model names (were plural)
* Added more structure helpers

# 1.4.1

* Collection::lists was deprecated and is now removed. Use "::pluck" instead.
* Dont list static methods as actions
* Add Viewcomposer for all layouts in configured path

# 1.4.0

* Query scope for online models
* Fixed bug where Head-title that was already set, was overwritten
