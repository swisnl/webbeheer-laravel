<?php

namespace WebbeheerLaravel\Commands;

use Illuminate\Console\Command;
use WebbeheerLaravel\Helpers\FindLayoutZones;

class FindLayourZones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webbeheer-laravel:find-layout-zones {layout} {format=console : Output the results in console or json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search the app for zones in layouts';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = FindLayoutZones::search($this->argument('layout'));

        if($this->argument('format') == 'console'){
            foreach($result as $key => $value){
                $result[$key]['zones'] = implode(', ', $value['zones']);
            }
            $this->table(['name', 'zones'], $result);
        } else {
            $this->comment(json_encode($result));
        }
    }
}
