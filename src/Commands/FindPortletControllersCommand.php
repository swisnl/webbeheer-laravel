<?php

namespace WebbeheerLaravel\Commands;

use Illuminate\Console\Command;
use WebbeheerLaravel\Helpers\FindPortletControllers;

class FindPortletControllersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webbeheer-laravel:find-controllers {format=console : Output the results in console or json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search the app for portlet controllers and their allowed methods';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = FindPortletControllers::search();

        if($this->argument('format') == 'console'){
            $this->table(array_keys($result[0]), $result);
        } else {
            $this->comment(json_encode($result));
        }
    }
}
