<?php

namespace WebbeheerLaravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateWebbeheerUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webbeheer-laravel:create-webbeheer-user {--gebruikersnaam=} {--voorletters=} {--achternaam=} {--emailadres=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an admin user';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (App::environment('local')) {

            $basisGebruiker = DB::table('gebruikers')->select('id')->where('gebruikersgroep', 1)->first()->id;

            $this->info('Gebruiker aanmaken voor Webbeheer');

            $gebruikersnaam = $this->option('gebruikersnaam');
            $voorletters = $this->option('voorletters');
            $achternaam = $this->option('achternaam');
            $emailadres = $this->option('emailadres');

            $gebruikersgroep = 1;
            if ($gebruikersnaam === null) {
                $gebruikersnaam = $this->ask('Gebruikersnaam');
            }
            if ($voorletters === null) {
                $voorletters = $this->ask('Voorletters');
            }
            if ($achternaam === null) {
                $achternaam = $this->ask('Achternaam');
            }
            if ($emailadres === null) {
                $emailadres = $this->ask('emailadres');
            }

            if(substr_count($emailadres, '@') === 1 && Str::endsWith($emailadres, '@swis.nl') === false){
                $this->error('Je kan alleen een gebruiker@swis.nl aanmaken.');
                return;
            }

            $gebruiker = [
                'gebruikersnaam' => $gebruikersnaam,
                'gebruikersgroep' => $gebruikersgroep,
                'voorletters' => $voorletters,
                'achternaam' => $achternaam,
                'emailadres' => $emailadres,
            ];

            if (Schema::hasColumn('gebruikers', 'wachtwoord_pbkdf2')) {
                $gebruiker['wachtwoord_pbkdf2'] = '';
            }

            $result = DB::table('gebruikers')
                ->insertGetId(
                    $gebruiker
                );

            if ($result === false) {
                $this->error('Aanmaken van de gebruiker is mislukt');

                return;
            }

            $gebruikersRechten = DB::table('gebruikersrechten')->select(
                [
                    DB::raw((int)$result.' AS gebruiker'),
                    'module',
                    'lezen',
                    'toevoegen',
                    'wijzigen',
                    'verwijderen',
                ]
            )->where('gebruiker', (int)$basisGebruiker)->get();
            $gebruikersRechten = array_map(
                function ($value) {
                    return (array)$value;
                },
                $gebruikersRechten->toArray()
            );

            $resultRechten = DB::table('gebruikersrechten')->insert($gebruikersRechten);

            if ($resultRechten === false) {
                $this->error('Invoegen van gebruikersrechten is mislukt.');
                DB::table('gebruiker')->where('id', $result)->delete();
                return;
            }

            $this->info('Gebruiker is aangemaakt, reset je wachtwoord om in te kunnen loggen.');

        }
    }
}
