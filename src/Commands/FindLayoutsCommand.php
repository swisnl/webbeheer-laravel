<?php

namespace WebbeheerLaravel\Commands;

use Illuminate\Console\Command;
use WebbeheerLaravel\Helpers\FindLayouts;

class FindLayoutsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webbeheer-laravel:find-layouts {format=console : Output the results in console or json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search the app for layouts';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = FindLayouts::search();

        if($this->argument('format') == 'console'){
            $this->table(['path'], $result);
        } else {
            $this->comment(json_encode($result));
        }
    }
}
