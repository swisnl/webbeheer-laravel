<?php

namespace WebbeheerLaravel\ViewComposers;

use Head;
use Illuminate\Contracts\View\View;
use WebbeheerLaravel\Structure\Model\Node;

class HeadHtmlComposer
{

    /**
     * HeadHtmlComposer constructor.
     *
     * @param \WebbeheerLaravel\Structure\Model\Node $node
     */
    public function __construct(Node $node)
    {
        $this->setTitle($node);
        $this->setMetaDescription($node);
    }

    /**
     * @param \WebbeheerLaravel\Structure\Model\Node $node
     */
    protected function setTitle(Node $node)
    {
        if (null === $node->id) {
            return;
        }

        if (\Head::hasTitle()) {
            return;
        }

        $klantnaam = env('KLANTNAAM');

        if ($klantnaam) {
            Head::setTitle($node->title.' - '.$klantnaam);
        } else {
            Head::setTitle($node->title);
        }
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        Head::addCss([
            'dist/css/screen' => 'screen',
            'dist/css/print' => 'print',
        ]);
        Head::addScript([
            'dist/js/app' => 'load',
        ]);


        $view->with('headHtml', view('webbeheer-laravel::portlets.head-html.head-html', ['headHtml' => Head::render()]));
    }

    /**
     * @param \WebbeheerLaravel\Structure\Model\Node $node
     */
    protected function setMetaDescription(Node $node)
    {
        if (null === $node->id) {
            return;
        }

        if($node->description == '') {
            return;
        }

        \Head::setDescription($node->description);
    }
}