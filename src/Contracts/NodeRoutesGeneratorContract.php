<?php

namespace WebbeheerLaravel\Contracts;

interface NodeRoutesGeneratorContract
{

    public function getNodesData(): array;

    public function registerNodesRoutes();

    public function registerNodeRoute($node);

}