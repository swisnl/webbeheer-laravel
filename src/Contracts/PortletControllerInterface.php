<?php
namespace WebbeheerLaravel\Contracts;

interface PortletControllerInterface
{
    /**
     * @var array Public methods that are hidden for portlets
     */
    public static function getHiddenActions();

    /**
     * @todo Idea to restrict controller to certain layouts
     * Restrict the controller to only certain blocks or layout?
     * standaard.middleColumn|rightColumn
     *
     * @var array Restrict controller to certain layouts and blocks
     */
    // public static $restictBlocks = [];


}