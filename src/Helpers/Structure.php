<?php

namespace WebbeheerLaravel\Helpers;

use WebbeheerLaravel\Structure\Model\Content;
use WebbeheerLaravel\Structure\Model\Node;
use WebbeheerLaravel\Structure\Model\NodesRepository;
use WebbeheerLaravel\Structure\Model\Parameter;

class Structure {

    /**
     * @param int $nodeId
     * @param array $parameters
     * @param bool $absolute
     * @return string
     */
    public static function getPathToNode(int $nodeId, array $parameters = [], bool $absolute = true) : string
    {
        $node = Node::find($nodeId);

        $routeName = $node->route_name;

        if(empty($node->route_name)) {
            $routeName = NodesRepository::getDefaultRouteToNode($nodeId);

            if($routeName === false){
                return '';
            }
        }

        return route($routeName, $parameters, $absolute);
    }

    /**
     * @param string $module
     * @param string $view
     * @param array $parameters
     * @param bool $absolute
     * @return string
     */
    public static function getPathByModuleAndView(string $module, string $view, array $parameters = [], bool $absolute = true) : string
    {
        $content = Content::where('view_script', $view)->whereHas('module', function($query) use ($module){
            $query->where('name', $module);
        })->first();

        return route($content->node->route_name, $parameters, $absolute);
    }

    /**
     * @param int $nodeId
     * @return Node
     */
    public static function getNodeById(int $nodeId)
    {
        return Node::findOrFail($nodeId);
    }

    /**
     * This function gets the node on which the element you are looking for is placed.
     *
     * @param string $module
     * @param null $id
     * @return Node
     */
    public static function getNodeByPortletContent(string $module, $id = null)
    {
        $parameter = Parameter::where('parameter', $module.'_id')->where('waarde', $id)->first();
        if(!$parameter || !$parameter->contents) {
            return null;
        }

        return $parameter->contents->node;
    }


    /**
     * @desc Get path to node WITH all info on nodes in path.
     *
     * @param int $iNodeId Node Id
     *
     * @return array Path to node with information ( array( 0 => aNodeInfo, 1 => aNodeInfo ) )
     */
    public static function getPathToNodeWithInfo($iNodeId) {


        $sSql = '   SELECT     p.*
                    FROM       sm_nodes p FORCE INDEX(lft_rgt)
                    LEFT JOIN  sm_nodes n
                    ON         n.id = ?
                    WHERE      p.lft <= n.lft
                    AND        p.rgt >= n.rgt
                    ORDER BY   p.lft ASC
                ';

        $aPath = \DB::select($sSql, [$iNodeId]);


        // root en de map eraf halen
        array_shift($aPath);
        array_shift($aPath);

        $aCleanPath = array();
        foreach ( $aPath as $key => $aNodeObject ) {
            $aNode = (array) $aNodeObject;
            $aCleanPath[$key] = $aNode;
            $aCleanPath[$key]['title'] = $aNode['title'];
            $aCleanPath[$key]['name'] = $aNode['name'];
        }

        return $aCleanPath;

    }
}
