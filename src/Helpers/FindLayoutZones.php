<?php

namespace WebbeheerLaravel\Helpers;

use Symfony\Component\Finder\Finder;

class FindLayoutZones {

    public static function search(string $layout){
        $layoutPath = config('webbeheer-laravel.view_layouts_folder');

        /**
         * @var $factory \Illuminate\View\Factory
         */
        $viewFinder = view()->getFinder();

        // Get base view paths
        $paths = $viewFinder->getPaths();

        // Get vendor paths to enable layouts in packages.
        $vendorNamespacedPaths = $viewFinder->getHints();
        foreach($vendorNamespacedPaths as $vendorPaths){
            $paths = array_merge($paths, $vendorPaths);
        }

        $found = [];
        foreach($paths as $viewPath) {
            if(!file_exists($viewPath.'/'.$layoutPath)){
                continue;
            }

            $finder = Finder::create()->files()->in($viewPath.'/'.$layoutPath)->name($layout . ".blade.php")->name($layout . '.twig');

            foreach($finder as $file) {

                $matches = [];
                preg_match_all('/{\!\![\s]?\@\$\K([a-zA-Z0-9]+)[\s]?\!\!\}/', $file->getContents(), $matches);

                if(isset($matches[1]) && count($matches[1]) > 0){
                    $found[] = ['name' => $layout, 'zones' => $matches[1]];
                }

            }

        }

        return $found;
    }

}
