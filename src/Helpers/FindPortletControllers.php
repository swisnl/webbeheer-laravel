<?php

namespace WebbeheerLaravel\Helpers;

use Composer\Autoload\ClassLoader;
use WebbeheerLaravel\Structure\Resolvers\ContentControllerResolver;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class FindPortletControllers {

    public static function search(){

        $controllerNamespace = config('webbeheer-laravel.module_controller_namespace');
        $controllerDirectory = base_path(config('webbeheer-laravel.module_controller_path'));

        /**
         * Find all files names *Controller.php
         * @var $finder SplFileInfo[]
         */
        $finder = Finder::create()->files()->in($controllerDirectory)->name("*Controller.php");
        $found = [];
        foreach($finder as $file){
            // Skip everything in a subfolder for now.
            if($file->getRelativePath() !== ""){
                continue;
            }

            $controllerClass = $controllerNamespace . '\\' . $file->getBasename('.php');
            $controllerReflectionClass = new \ReflectionClass($controllerClass);

            $isPortletController = $controllerReflectionClass->implementsInterface('WebbeheerLaravel\Contracts\PortletControllerInterface');
            if($isPortletController){
                $methods = [];
                foreach($controllerReflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC) as $method){
                    if ($method->isStatic() || $method->isConstructor() || $method->isInternal()) {
                        continue;
                    }

                    // Check if the method is defined in this class, maybe change to anything defined in the app so you
                    // can reuse actions?
                    if($method->class === $controllerClass && !in_array($method->name, $controllerClass::getHiddenActions())){
                        $methods[] = $method->name;
                    }
                }

                $found[] = [
                    'module' => ContentControllerResolver::moduleName($controllerClass),
                    'class' => $controllerClass,
                    'methods' => implode(',', $methods)
                ];
            }
        }
        return $found;
    }

}