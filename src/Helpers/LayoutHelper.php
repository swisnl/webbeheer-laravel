<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 2016-01-21
 * Time: 22:45
 */

namespace WebbeheerLaravel\Helpers;


use WebbeheerLaravel\Structure\Model\Block;
use WebbeheerLaravel\Structure\Model\Layout;

class LayoutHelper
{
    protected $blocks = [];

    /**
     * LayoutHelper constructor.
     * @param Layout $layout
     */
    public function __construct(Layout $layout)
    {
        $this->layout = $layout;

        $this->blocks['renderLog'] = 'Log for: ' . $layout->name . ' - ';
        foreach($layout->blocks as $block){
            if(!isset($this->blocks[$block->tag])){
                $this->blocks[$block->tag] = '';
            }
            $this->blocks['renderLog'] .= $block->tag . ', ';
        }
    }

    /**
     * Add a piece of content to a block
     *
     * @param Block $block
     * @param $content
     */
    public function addContent(Block $block, $content){
        if(!isset($this->blocks[$block->tag])){
            $this->blocks[$block->tag] = '';
        }
        $this->blocks[$block->tag] .= $content;
    }

    /**
     * Add a piece of content to a block
     *
     * @param string $tag
     * @param string $content
     */
    public function addRawContent($tag, $content){
        if(!isset($this->blocks[$tag])){
            $this->blocks[$tag] = '';
        }
        $this->blocks[$tag] .= $content;
    }



    /**
     * @return array
     */
    public function blocks(){
        return $this->blocks;
    }


}