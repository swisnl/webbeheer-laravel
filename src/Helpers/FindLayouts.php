<?php

namespace WebbeheerLaravel\Helpers;

use Symfony\Component\Finder\Finder;

class FindLayouts {

    public static function search(){
        $layoutPath = config('webbeheer-laravel.view_layouts_folder');

        /**
         * @var $factory \Illuminate\View\Factory
         */
        $viewFinder = view()->getFinder();

        // Get base view paths
        $paths = $viewFinder->getPaths();

        // Get vendor paths to enable layouts in packages.
        $vendorNamespacedPaths = $viewFinder->getHints();
        foreach($vendorNamespacedPaths as $vendorPaths){
            $paths = array_merge($paths, $vendorPaths);
        }

        $found = [];
        foreach($paths as $viewPath) {
            if(!file_exists($viewPath.'/'.$layoutPath)){
                continue;
            }
            $finder = Finder::create()->files()->in($viewPath.'/'.$layoutPath)->name("*.blade.php")->name('*.twig');

            foreach($finder as $file) {
                $found[] = ['path' => $file->getRealpath()];
            }

        }

        return $found;
    }

}