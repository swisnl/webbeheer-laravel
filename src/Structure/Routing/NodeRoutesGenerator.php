<?php

namespace WebbeheerLaravel\Structure\Routing;


use Illuminate\Support\Facades\Route;
use WebbeheerLaravel\Contracts\NodeRoutesGeneratorContract;
use WebbeheerLaravel\Structure\Model\NodesRepository;

class NodeRoutesGenerator implements NodeRoutesGeneratorContract
{
    public function getNodesData(): array
    {
        $nodes = NodesRepository::all();

        $nodeData = [];
        $firstNode = true;
        $stack = array();

        foreach ($nodes as $node) {

            if ($node->depth <= 0) {
                // Root node en containers level 1 overslaan
                continue;
            }

            if ($node->depth == count($stack)) {
                array_pop($stack);
            }

            if ($node->depth < count($stack)) {
                $iCalculatedDept = count($stack) - $node->depth + 1;
                while ($iCalculatedDept--) {
                    array_pop($stack);
                }
            }

            // Node naam in stack
            array_push($stack, $node->name);

            $stackToNode = array_slice($stack, 0, $node->depth);

            $routeName = empty($node->route_name) ? implode('.', $stackToNode) : $node->route_name;

            if ((int)$node->special_page === 1) {
                // Remove last and and $key as route component
                array_pop($stackToNode);
                $stackToNode[] = '{slug}';
            }
            $routeUrl = implode('/', $stackToNode);

            if ($firstNode) {
                $nodeData[] = ['url' => '/', 'name' => $routeName, 'id' => $node->id];
                $firstNode = false;
                continue;
            }

            $nodeData[] = ['url' => $routeUrl, 'name' => $routeName, 'id' => $node->id];

        }

        return $nodeData;
    }

    public function registerNodesRoutes()
    {
        foreach ($this->getNodesData() as $node) {
            $this->registerNodeRoute($node);
        }
    }

    public function registerNodeRoute($node)
    {
        Route::match(['get', 'post'], $node['url'], [
            'as' => $node['name'],
            'uses' => config('webbeheer-laravel.layout_controller_route'),
            'node_id' => $node['id'],
            'middleware' => config('webbeheer-laravel.node_routes_middleware'),
        ]);
    }
}
