<?php

namespace WebbeheerLaravel\Structure;


use Illuminate\Support\Collection;
use WebbeheerLaravel\Helpers\Structure;
use WebbeheerLaravel\Structure\Model\Node;

class Navigation {

    protected $nodes = array();

    /**
     * @desc Base url for root node to make relative paths possible
     */
    public $rootUrl;

    private   $rootId;
    private   $rootName;
    private   $rootTitle;
    private   $rootLeft;
    private   $rootRight;
    private   $showHomepage;
    private   $currentNodeId;
    private   $currentPath;

    private   $homepageNodeId = 3;

    private   $showHidden;
    private   $showOffline;


    private $shortcut;
    private $recursive;
    private $getActiveSubnodesOnly;

    private $lastActiveNodeId;

    public function __construct()
    {
        $this->rootId = 1;
        $this->rootName = 'root';
        $this->rootTitle = 'root';
        $this->rootLeft = 0;
        $this->rootRight = 0;
        $this->showHomepage = true;
        $this->currentNodeId = false;
        $this->currentPath = array();
        $this->sUpdateTime = null;
        $this->bUseSession = false;
        $this->oNavSession = null;
        $this->showHidden = true;
        $this->showOffline = false;
        $this->shortcut = false;

        $this->recursive = false;

        $this->getActiveSubnodesOnly = false;
    }

    /**
     * @desc Root id opgeven
     * @param integer Id van root
     */
    public function setRootId($id) {

        $this->rootId = $id;

        // retrieve the left and right value of the $root node
        $rootNode = Node::find($id);

        $this->rootName = $rootNode['name'];
        $this->rootTitle = $rootNode['title'];
        $this->rootLeft = $rootNode['lft'];
        $this->rootRight = $rootNode['rgt'];

        $this->rootUrl = \WebbeheerLaravel\Helpers\Structure::getPathToNode($id) . '/';


    }

    /**
     * root opgeven o.b.v. name
     * @param string $name
     */
    public function setRootByName($name) {
        $this->setRootId(
            Node::where('name', '=', $name)
                ->first()->id
        );
    }


    /**
     * @desc Root id opgeven
     * @param integer Id van root
     */
    public function setCurrentNode($id) {

        $this->currentNodeId = $id;

        $this->getActiveIds($this->currentNodeId);

    }


    /**
     * @desc home knop in menu weergeven ja/nee
     * @param boolean true/false
     */
    public function showHomepage($show = true) {

        $this->showHomepage = $show;

    }


    /**
     * set flag of er recursief getoond moet worden
     * true = alle childnodes worden ook opgehaald
     * false = er worden geen childnodes opgehaald
     *
     * @param boolean $recursive
     */
    public function setRecursive($recursive = true) {
        $this->recursive = $recursive;
    }


    public function setGetActiveSubnodesOnly($activeNodesOnly = true) {

        if($activeNodesOnly !== false) {
            $this->getActiveSubnodesOnly = true;
        }

    }

    /**
     * @desc set false om de "Linkt Naar" link direct weer te geven in de plaats van de node link.
     * @desc Hiermee voorkom je een dubbele bootstrap bij een redirect met Linkt Naar.
     *
     * @param boolean $shortcut
     */
    public function setShortcutNodesDirect($shortcut = false) {
        if($shortcut === true) {
            $this->shortcut = true;
        }
    }


    /**
     * @desc verborgen nodes tonen ja/nee
     * @param boolean true/false
     */
    public function showHidden($show = true) {

        if($show === true) {
            $this->showHidden = true;
        } else {
            $this->showHidden = false;
        }

    }

    /**
     * @desc offline nodes tonen ja/nee
     * @param boolean true/false
     */
    public function showOffline($show = false) {

        if($show === true) {
            $this->showOffline = true;
        } else {
            $this->showOffline = false;
        }

    }

    private function parseHidden ($array, $setHidden = false) {

        if ($setHidden) {
            foreach ($array as $key => $data) {

                if (isset($data['childnodes']) && count($data['childnodes'])) {
                    $array[$key]['childnodes'] = $this->parseHidden($data['childnodes'], true);
                }
            }
        } else {
            foreach ($array as $key => $data) {
                if ($data['hidden'] == '1') {
                    $array[$key]['childnodes'] = $this->parseHidden($data['childnodes'], true);
                }
            }
        }


        return $array;

    }



    public function getNavigatie() {

        $this->nodes = $this->getNodes($this->rootLeft, $this->rootRight);

        $this->nodes = $this->parseHidden($this->nodes);

        if($this->getActiveSubnodesOnly) {

            $this->getActiveSubnodesOnly();

        }

        return $this->nodes;

    }




    public function getActiveSubnodesOnly() {

        foreach($this->nodes as $node) {
            if($node['active'] == 1) {
                $this->nodes = $node['childnodes'];
                return true;
            }
        }

    }




    /**
     * @desc Node ophalen uit database o.b.v. de opgegeven root
     * @return array of nodes
     */
    private function getNodes($left, $right) {

        // retrieve descendants
        $sql = "	SELECT 		*,
								0 AS active,
								0 as parent_id
					FROM 		sm_nodes    FORCE INDEX(lft_rgt)
					WHERE 		special_page = 0
                    AND         lft > ?
					AND 		lft < ?
					ORDER BY 	lft ASC";
        $descendants = \DB::select($sql, [$left, $right]);

        $nodes = array();

        if(!empty($descendants)) {

            // Homepage uit array verwijderen
            if(!$this->showHomepage) {
                array_shift($descendants);
            }

            // start with an empty $right stack
            $right = array();

            // Return array for the nodes
            $nodes = array();

            // display each row
            foreach($descendants as $rowObject) {
                $row = (array) $rowObject;

                // key maken o.b.v. titel
                $this->setKey($row);

                // is active of niet
                $this->setActiveState($row);

                // link naar node maken
                $this->setLinkToNode($row);


                // only check stack if there is one
                if (count($right)>0) {
                    // check if we should remove a node from the stack
                    while(count($right) > 0 && $right[count($right)-1] < $row['rgt']) {

                        array_pop($right);

                    }
                }


                if(count($right) > 0) {

                    $lastNodes[count($right)+1] = $row;

                    // bij recursief moeten de childnodes opgehaald worden
                    if($this->recursive) {
                        $this->setChildren($nodes, $right, $row);
                    }



                } else {
                    $lastNodes[count($right)+1] = $row;
                    $nodes[$row['rgt']] = $row;

                    // is active of niet
                    $this->setLastActiveNode($row);

                    if(!isset($nodes[$row['rgt']]['childnodes'])) {
                        $nodes[$row['rgt']]['childnodes'] = array();
                    }
                }

                // add this node to the stack
                $right[] = $row['rgt'];

            }

        }

        if($this->showHidden === false || $this->showOffline === false ) {
            $nodes = $this->removeHiddenNodes($nodes);
        }

        return $nodes;
    }

    private function removeHiddenNodes($nodes){
        foreach($nodes as $key => $value){

            if($value['hidden'] == 1 && $this->showHidden === false){
                unset($nodes[$key]);
            } elseif($value['online'] == 0 && $this->showOffline === false) {
                unset($nodes[$key]);
            } elseif(isset($value['childnodes']) && count($value['childnodes']) > 0){
                $nodes[$key]['childnodes'] = $this->removeHiddenNodes($nodes[$key]['childnodes']);
            }
        }

        return $nodes;
    }



    /**
     * @desc kijken of deze node in het pad naar de current node zit of de current node is
     * @param array node
     */
    private function setActiveState(&$node) {

        if(in_array($node['id'], $this->currentPath)) {
            $node['active'] = 1;
        } else {
            $node['active'] = 0;
        }

    }


    /**
     * @desc kijken of deze node in het pad naar de current node zit of de current node is
     * @param array node
     */
    private function setLastActiveNode($node) {

        if(in_array($node['id'], $this->currentPath)) {
            $this->lastActiveNodeId = $node['id'];
        }

    }



    /**
     * @desc de key van deze node o.b.v. z'n naam setten
     * @param array node
     */
    private function setKey(&$node) {

        $node['key'] = $node['name'];

    }



    /**
     * @desc de link naar deze node setten
     * @param array node
     */
    private function setLinkToNode(&$node) {

        // Als het een dynamische url is dan wil je geen link.
        if(strpos($node['name'], '{') !== false) {
            $node['link'] = '';
        }else if($this->shortcut === true && $node['shortcut_to'] != 0 && $node['shortcut_to'] != '') {
            $node['link'] = Structure::getPathToNode($node['shortcut_to']);
        } else {
            $node['link'] = Structure::getPathToNode($node['id']);
        }
    }

    /**
     * @desc haal current path op
     *
     */
    public function getCurrentPath(){
        return $this->currentPath;
    }


    /**
     * @desc maak array met id's van de nodes uit het pad naar de huidige node
     * @param integer id van huidige node
     */
    private function getActiveIds($nodeId) {

        $sSql = '	SELECT 		p.id
    				FROM 		sm_nodes p FORCE INDEX(lft_rgt)
    				LEFT JOIN	sm_nodes n
    				ON			n.id = ' . (int) $nodeId . '
    				WHERE 		p.lft <= n.lft
    				AND 		p.rgt >= n.rgt
    				AND			p.lft > ' . $this->rootLeft . '
    				AND			p.lft < ' . $this->rootRight . '
    				ORDER BY 	p.lft ASC
    			';
        if(!isset($this->currentPath) || count($this->currentPath) == 0) {
            $this->currentPath = array();
        }
        $this->currentPath = array_merge($this->currentPath, (new Collection(\DB::select($sSql)))->pluck('id')->toArray());
    }

    /**
     * @desc zet shortcuts naar huidige node ook op active
     *
     * @param integer id van huidige node
     */
    public function setShortcutNodesActive($nodeId) {
        // haal alle pagina's op tot de huidige node
        $nodes = Structure::getPathToNodeWithInfo($nodeId);

        // geen pagina's gevonden?
        if(!count($nodes)) {
            return;
        }

        // haal alle ids op
        $nodeIds = array();
        foreach($nodes as $node) {
            $nodeIds[] = $node['id'];
        }

        // plaats alle shortcut to nodes ook in de currentPath
        $sSql = '	SELECT 		id
    				FROM 		sm_nodes
    				WHERE		shortcut_to IN (' .implode(',', $nodeIds). ')
    				ORDER BY 	lft ASC
				';
        if(!isset($this->currentPath) || count($this->currentPath) == 0) {
            $this->currentPath = array();
        }

        $this->currentPath = array_merge($this->currentPath, (new Collection(\DB::select($sSql)))->pluck('id')->toArray());
    }




    /**
     * @desc Recursively add childnodes based on array of RIGHT id's
     * @param $node The nodelijst waar de ROW in toegevoegd moet worden.
     * @param $right Een array met de huidige nodeid stack (dus genest de id's van de nodes naar de huidige node)
     * @param $row De rij (node) die in de nodelijst toegevoegd moet worden
     */
    private function setChildren(&$node, $right, $row){

        // is active of niet
        $this->setActiveState($row);

        if ( count($right) == 1 ) {
            $row['parent_id'] = $right[0];
            $node[$right[0]]['childnodes'][$row['rgt']] = $row;
        } else {
            $currentNod = array_shift($right);
            $this->setChildren($node[$currentNod]['childnodes'], $right, $row);
        }
    }


    /**
     * @desc Get submenu from given dept.
     * @param int $id
     * @param int $parentDepth How deep is the parent of this submenu. Default: 1 - Parents are first nodes.
     * @return array Node based on parameters
     */
    public function getSubMenu($id, $parentDepth = 1){

        $aPath = Structure::getPathToNodeWithInfo($id);

        while ( $parentDepth-- ) {
            $parentNode = array_shift($aPath);
        }

        $this->rootUrl = Structure::getPathToNode($parentNode['id']). '/';

        return $this->getNodes($parentNode['lft'], $parentNode['rgt']);
    }



    public function getLastActiveNodeId() {
        return $this->lastActiveNodeId;
    }


}
