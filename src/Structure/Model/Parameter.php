<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

/**
 * \WebbeheerLaravel\Structure\Model\Content
 *
 * @property string $parameter
 * @property string $waarde
 * @property string $titel
 * @property string $label
 * @property-read \WebbeheerLaravel\Structure\Model\Content $contents
 * @property integer $sm_contents_id
 */
class Parameter extends Model
{

    use Rememberable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sm_contents_parameters';

    public function contents()
    {
        return $this->belongsTo(Content::class, 'sm_contents_id')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

}
