<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

/**
 * WebbeheerLaravel\Structure\Model
 *
 * @property integer $id
 * @property integer $gebruiker
 * @property integer $groep
 * @property integer $layout_id
 * @property string $tag
 * @property string $titel
 * @property float $volgorde
 * @property boolean $niet_tonen
 */
class Navigation extends Model
{

    use Rememberable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sm_navigation';

}
