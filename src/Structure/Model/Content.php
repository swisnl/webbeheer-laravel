<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

/**
 * \WebbeheerLaravel\Structure\Model\Content
 *
 * @property integer $id
 * @property integer $node_id
 * @property integer $block_id
 * @property integer $module_id
 * @property boolean $is_main
 * @property boolean $volgorde
 * @property boolean $is_hidden
 * @property string $view_script
 * @property integer $parent_node
 * @property-read \WebbeheerLaravel\Structure\Model\Module $module
 * @property-read \WebbeheerLaravel\Structure\Model\Block $block
 * @property-read \WebbeheerLaravel\Structure\Model\Parameter $parameters
 */
class Content extends Model
{

    use Rememberable;

    protected $allParameters;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sm_contents';

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

    public function block()
    {
        return $this->belongsTo(Block::class, 'block_id')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

    public function parameters()
    {
        return $this->hasMany(Parameter::class, 'sm_contents_id', 'id')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

    public function node()
    {
        return $this->belongsTo( Node::class, 'node_id');
    }

    /**
     * Get parameter by name
     *
     * @param $param
     * @return null|string
     */
    public function getParameter($param){
        $this->loadParameters();
        if(isset($this->allParameters[$param])){
            return $this->allParameters[$param];
        } else {
            return null;
        }
    }

    /**
     * Get all content parameters
     *
     * @return array
     */
    public function getAllParameters(){
        $this->loadParameters();
        return isset($this->allParameters) ? $this->allParameters : [];
    }

    protected function loadParameters()
    {
        if (!isset($this->allParameters)) {
            $this->allParameters = $this->parameters->pluck('waarde', 'parameter');
        }
    }
}
