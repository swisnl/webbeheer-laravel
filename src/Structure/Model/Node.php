<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

/**
 * WebbeheerLaravel\Structure\Model\Node
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $route_name
 * @property integer $shortcut_to
 * @property integer $layout_id
 * @property integer $lft
 * @property integer $rgt
 * @property boolean $max_childnodes
 * @property boolean $max_depth
 * @property boolean $locked
 * @property boolean $special_page
 * @property boolean $hidden
 * @property boolean $online
 * @property string $keywords
 * @property string $description
 * @property integer $gebruiker
 * @property integer $groep
 * @property string $pagina_titel
 * @property-read \Illuminate\Database\Eloquent\Collection|\WebbeheerLaravel\Structure\Model\Content[] $contents
 * @property-read \WebbeheerLaravel\Structure\Model\Layout $layout
 */
class Node extends Model
{

    use Rememberable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sm_nodes';


    public function contents()
    {
        return $this->hasMany(Content::class, 'node_id', 'id')->orderBy('volgorde')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

    public function visibleContents()
    {
        return $this->contents()->where('is_hidden', '<>', 1)->get();
    }


    public function layout()
    {
        return $this->belongsTo(Layout::class, 'layout_id')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

}
