<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Support\Facades\DB;

class NodesRepository
{

    /**
     * @return Node[]
     */
    public static function all()
    {

        return DB::select(
            '
            SELECT 	MIN(node.id) AS id, 
                        MIN(node.name) AS name, 
                        MIN(node.route_name) AS route_name, 
                        MIN(node.locked) AS locked, 
                        (COUNT(parent.name) - 2) AS depth, 
                        MIN(node.shortcut_to) AS shortcut_to, 
                        MIN(node.special_page) AS special_page
            FROM 		sm_nodes AS node, 
                        sm_nodes AS parent
            WHERE 	node.lft >= parent.lft 
            AND 		node.lft <= parent.rgt
            GROUP BY node.lft
            ORDER BY node.lft
            '
        );
    }

    public static function getDefaultRouteToNode(int $nodeId){
        static $nodeRouteCache = array();

        if(isset($nodeRouteCache[$nodeId])){
            return $nodeRouteCache[$nodeId];
        }

        $sql = "        SELECT     p.name
		                FROM       sm_nodes p FORCE INDEX(lft_rgt)
		                LEFT JOIN  sm_nodes n
		                ON         n.id = ". (int) $nodeId ."
		                WHERE      p.lft <= n.lft
		                AND        p.rgt >= n.rgt
		                ORDER BY   p.lft ASC
                    ";

        $path = DB::select($sql);

        // root en de map eraf halen
        array_shift($path);
        array_shift($path);

        if(count($path) === 0){
            return false;
        }

        $names = [];
        foreach($path as $node){
            $names[] = $node->name;
        }

        $nodePathCache[$nodeId] = implode('.', $names);

        return $nodePathCache[$nodeId];
    }
}
