<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

/**
 * WebbeheerLaravel\Structure\Model\Layout
 *
 * @property integer $id
 * @property integer $gebruiker
 * @property integer $groep
 * @property string $name
 * @property string $titel
 * @property string $thumbnail
 * @property float $volgorde
 * @property string $niet_tonen
 * @property-read \Illuminate\Database\Eloquent\Collection|\WebbeheerLaravel\Structure\Model\Block[] $blocks
 */
class Layout extends Model
{

    use Rememberable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sm_layouts';

    public function blocks()
    {
        return $this->hasMany(Block::class, 'layout_id', 'id')->remember(config('webbeheer-laravel.structure_eloquent_model_cache_ttl') * 60);
    }

}
