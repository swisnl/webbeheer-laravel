<?php
namespace WebbeheerLaravel\Structure\Model;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

/**
 * WebbeheerLaravel\Structure\Model\Module
 *
 * @property integer $id
 * @property integer $gebruiker
 * @property integer $groep
 * @property string $name
 * @property string $title
 * @property string $child_pages
 */
class Module extends Model
{

    use Rememberable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sm_modules';

}
