<?php
namespace WebbeheerLaravel\Structure\Resolvers;

use Illuminate\Support\Str;
use WebbeheerLaravel\Structure\Model\Content;

class ContentControllerResolver implements ContentControllerResolverInterface
{
    /**
     * Resolve the content module to a controller name
     *
     * @param Content $content
     * @return string
     */
    public static function controllerName(Content $content)
    {
        return config('webbeheer-laravel.module_controller_namespace') . '\\'. Str::ucfirst(Str::camel($content->module->name . 'Controller'));
    }

    /**
     * Resolve the contents view-script to an action
     *
     * @param Content $content
     * @return string
     */
    public static function actionName(Content $content)
    {
        return Str::camel($content->view_script);
    }

    /**
     * Convert a controller class name to a module name
     *
     * @param $className
     * @return string
     */
    public static function moduleName($className){
        return Str::snake(str_replace([config('webbeheer-laravel.module_controller_namespace') . '\\', 'Controller'], '', $className));
    }
}