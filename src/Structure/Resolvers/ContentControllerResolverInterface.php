<?php
/**
 * Created by PhpStorm.
 * User: bjorn
 * Date: 2016-01-21
 * Time: 21:51
 */
namespace WebbeheerLaravel\Structure\Resolvers;

use WebbeheerLaravel\Structure\Model\Content;

interface ContentControllerResolverInterface
{
    /**
     * Resolve the content module to a controller name
     *
     * @param Content $content
     * @return string
     */
    public static function controllerName(Content $content);

    /**
     * Resolve the contents view-script to an action
     *
     * @param Content $content
     * @return string
     */
    public static function actionName(Content $content);

    /**
     * Convert a controller class name to a module name
     *
     * @param $className
     * @return string
     */
    public static function moduleName($className);
}