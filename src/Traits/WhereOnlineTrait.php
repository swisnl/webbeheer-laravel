<?php

namespace WebbeheerLaravel\Traits;

use WebbeheerLaravel\QueryScopes\WhereOnlineScope;

trait WhereOnlineTrait
{
    public static function bootWhereOnlineTrait()
    {
        static::addGlobalScope(new WhereOnlineScope());
    }
}