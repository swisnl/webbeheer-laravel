<?php

namespace WebbeheerLaravel\Database\Seeding;

class Module
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var int
     */
    private static $order = 0;

    /**
     * @param \WebbeheerLaravel\Database\Seeding\Seeder $seeder
     * @param string                                    $module
     * @param string                                    $name
     * @param int|string                                $category Id or name
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(Seeder $seeder, string $module, string $name, $category)
    {
        $this->seeder = $seeder;

        $this->data = [
            'gebruiker'               => '1',
            'groep'                   => '1',
            'module'                  => $module,
            'titel'                   => $name,
            'rubriek'                 => null,
            'selecteer_eerste_record' => false,
            'toon_in_menu'            => true,
            'volgorde'                => ++static::$order,
            'rechten_lezen'           => true,
            'rechten_wijzigen'        => true,
            'rechten_toevoegen'       => true,
            'rechten_verwijderen'     => true,
            'doorzoekbaar'            => true,
            'toon_in_dashboard'       => false,
            'aantal_overview'         => 25,
        ];

        $this->setCategory($category);
    }

    /**
     * @param array $users Id or username
     * @param int   $read
     * @param int   $create
     * @param int   $edit
     * @param int   $delete
     *
     * @throws \InvalidArgumentException
     *
     * @return $this
     */
    public function withUserPermissions(
        array $users,
        int $create = 1,
        int $read = 3,
        int $edit = 3,
        int $delete = 3
    ): self {
        if (!\is_array($users)) {
            throw new \InvalidArgumentException('Users should be an array.');
        }

        foreach ($users as $user) {
            $this->seeder->addUserPermission($this->data['module'], $user)
                ->setCreate($create)
                ->setRead($read)
                ->setUpdate($edit)
                ->setDelete($delete);
        }

        return $this;
    }

    /**
     * @param array $userGroups Id or name
     * @param int   $read
     * @param int   $create
     * @param int   $edit
     * @param int   $delete
     *
     * @throws \InvalidArgumentException
     *
     * @return $this
     */
    public function withUserGroupPermissions(
        array $userGroups,
        int $create = 1,
        int $read = 3,
        int $edit = 3,
        int $delete = 3
    ): self {
        if (!\is_array($userGroups)) {
            throw new \InvalidArgumentException('Usergroups should be an array.');
        }

        foreach ($userGroups as $userGroup) {
            $this->seeder->addUserGroupPermission($this->data['module'], $userGroup)
                ->setCreate($create)
                ->setRead($read)
                ->setUpdate($edit)
                ->setDelete($delete);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function create(): array
    {
        return array_map(
            function ($value) {
                return $value instanceof \Closure ? $value() : $value;
            },
            $this->data
        );
    }

    /**
     * @param string $module
     *
     * @return $this
     */
    public function setModule(string $module): self
    {
        $this->data['module'] = $module;

        return $this;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->data['module'];
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->data['titel'] = $title;

        return $this;
    }

    /**
     * @param int|string $category Id or name
     *
     * @throws \InvalidArgumentException
     *
     * @return $this
     */
    public function setCategory($category): self
    {
        if (is_numeric($category)) {
            $this->data['rubriek'] = $category;
        } else {
            $this->data['rubriek'] = function () use ($category) {
                $id = $this->seeder->getModuleCategoryIdByName($category);

                if ($id === null) {
                    throw new \InvalidArgumentException(sprintf('Could not find module category with name "%s"', $category));
                }

                return $id;
            };
        }

        return $this;
    }

    /**
     * @param bool $selectFirstRecord
     *
     * @return $this
     */
    public function setSelectFirstRecord(bool $selectFirstRecord): self
    {
        $this->data['selecteer_eerste_record'] = $selectFirstRecord;

        return $this;
    }

    /**
     * @param bool $showInMenu
     *
     * @return $this
     */
    public function setShowInMenu(bool $showInMenu): self
    {
        $this->data['toon_in_menu'] = $showInMenu;

        return $this;
    }

    /**
     * @param int $order
     *
     * @return $this
     */
    public function setOrder(int $order): self
    {
        $this->data['volgorde'] = $order;

        return $this;
    }

    /**
     * @param bool $permission
     *
     * @return $this
     */
    public function setPermissionCreate(bool $permission): self
    {
        $this->data['rechten_toevoegen'] = $permission;

        return $this;
    }

    /**
     * @param bool $permission
     *
     * @return $this
     */
    public function setPermissionRead(bool $permission): self
    {
        $this->data['rechten_lezen'] = $permission;

        return $this;
    }

    /**
     * @param bool $permission
     *
     * @return $this
     */
    public function setPermissionUpdate(bool $permission): self
    {
        $this->data['rechten_wijzigen'] = $permission;

        return $this;
    }

    /**
     * @param bool $permission
     *
     * @return $this
     */
    public function setPermissionDelete(bool $permission): self
    {
        $this->data['rechten_verwijderen'] = $permission;

        return $this;
    }

    /**
     * @param bool $searchable
     *
     * @return $this
     */
    public function setSearchable(bool $searchable): self
    {
        $this->data['doorzoekbaar'] = $searchable;

        return $this;
    }

    /**
     * @param bool $showOnDashboard
     *
     * @return $this
     */
    public function setShowOnDashboard(bool $showOnDashboard): self
    {
        $this->data['toon_in_dashboard'] = $showOnDashboard;

        return $this;
    }

    /**
     * @param int $amountPerPage
     *
     * @return $this
     */
    public function setAmountPerPage(int $amountPerPage): self
    {
        $this->data['aantal_overview'] = $amountPerPage;

        return $this;
    }
}
