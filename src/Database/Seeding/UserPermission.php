<?php

namespace WebbeheerLaravel\Database\Seeding;

class UserPermission
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param \WebbeheerLaravel\Database\Seeding\Seeder $seeder
     * @param string                                    $module
     * @param int|string                                $user Id or username
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(Seeder $seeder, string $module, $user)
    {
        $this->seeder = $seeder;

        $this->data = [
            'gebruiker'   => null,
            'module'      => $module,
            'lezen'       => 3,
            'toevoegen'   => 1,
            'wijzigen'    => 3,
            'verwijderen' => 3,
        ];

        $this->setUser($user);
    }

    /**
     * @return array
     */
    public function create(): array
    {
        return array_map(
            function ($value) {
                return $value instanceof \Closure ? $value() : $value;
            },
            $this->data
        );
    }

    /**
     * @param int|string $user Id or username
     *
     * @throws \InvalidArgumentException
     *
     * @return $this
     */
    public function setUser($user): self
    {
        if (is_numeric($user)) {
            $this->data['gebruiker'] = $user;
        } else {
            $this->data['gebruiker'] = function () use ($user) {
                $id = $this->seeder->getUserIdByUsername($user);

                if ($id === null) {
                    throw new \InvalidArgumentException(sprintf('Could not find a user with username "%s"', $user));
                }

                return $id;
            };
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->data['gebruiker'] instanceof \Closure ? $this->data['gebruiker']() : $this->data['gebruiker'];
    }

    /**
     * @param string $module
     *
     * @return $this
     */
    public function setModule(string $module): self
    {
        $this->data['module'] = $module;

        return $this;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->data['module'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setCreate(int $permission): self
    {
        $this->data['toevoegen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreate(): int
    {
        return $this->data['toevoegen'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setRead(int $permission): self
    {
        $this->data['lezen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getRead(): int
    {
        return $this->data['lezen'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setUpdate(int $permission): self
    {
        $this->data['wijzigen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdate(): int
    {
        return $this->data['wijzigen'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setDelete(int $permission): self
    {
        $this->data['verwijderen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getDelete(): int
    {
        return $this->data['verwijderen'];
    }
}
