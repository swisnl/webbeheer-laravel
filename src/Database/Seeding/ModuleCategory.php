<?php

namespace WebbeheerLaravel\Database\Seeding;

class ModuleCategory
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var int
     */
    private static $order = 0;

    /**
     * @param \WebbeheerLaravel\Database\Seeding\Seeder $seeder
     * @param string                                    $title
     * @param string                                    $description
     * @param string                                    $icon
     */
    public function __construct(Seeder $seeder, string $title, string $description, string $icon = 'content')
    {
        $this->seeder = $seeder;

        $this->data = [
            'gebruiker'         => '1',
            'groep'             => '1',
            'titel'             => $title,
            'volgorde'          => ++static::$order,
            'icoon'             => $icon,
            'icoon_glyph'       => 'glyphicon-'.$icon,
            'omschrijving'      => $description,
            'toon_in_menu'      => true,
            'toon_in_dashboard' => true,
        ];
    }

    /**
     * @return array
     */
    public function create(): array
    {
        return array_map(
            function ($value) {
                return $value instanceof \Closure ? $value() : $value;
            },
            $this->data
        );
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->data['titel'] = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->data['titel'];
    }

    /**
     * @param int $order
     *
     * @return $this
     */
    public function setOrder(int $order): self
    {
        $this->data['volgorde'] = $order;

        return $this;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon(string $icon): self
    {
        $this->data['icoon'] = $icon;

        return $this;
    }

    /**
     * @param string $glyph
     *
     * @return $this
     */
    public function setIconGlyph(string $glyph): self
    {
        $this->data['icoon_glyph'] = $glyph;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->data['omschrijving'] = $description;

        return $this;
    }

    /**
     * @param bool $showInMenu
     *
     * @return $this
     */
    public function setShowInMenu(bool $showInMenu): self
    {
        $this->data['toon_in_menu'] = $showInMenu;

        return $this;
    }

    /**
     * @param bool $showOnDashboard
     *
     * @return $this
     */
    public function setShowOnDashboard(bool $showOnDashboard): self
    {
        $this->data['toon_in_dashboard'] = $showOnDashboard;

        return $this;
    }
}
