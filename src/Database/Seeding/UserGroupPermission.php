<?php

namespace WebbeheerLaravel\Database\Seeding;

class UserGroupPermission
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param \WebbeheerLaravel\Database\Seeding\Seeder $seeder
     * @param string                                    $module
     * @param int|string                                $group Id or name
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(Seeder $seeder, string $module, $group)
    {
        $this->seeder = $seeder;

        $this->data = [
            'gebruikersgroep' => null,
            'module'          => $module,
            'lezen'           => 3,
            'toevoegen'       => 1,
            'wijzigen'        => 3,
            'verwijderen'     => 3,
        ];

        $this->setGroup($group);
    }

    /**
     * @return array
     */
    public function create(): array
    {
        return array_map(
            function ($value) {
                return $value instanceof \Closure ? $value() : $value;
            },
            $this->data
        );
    }

    /**
     * @param int|string $group Id or name
     *
     * @throws \InvalidArgumentException
     *
     * @return $this
     */
    public function setGroup($group): self
    {
        if (is_numeric($group)) {
            $this->data['gebruikersgroep'] = $group;
        } else {
            $this->data['gebruikersgroep'] = function () use ($group) {
                $id = $this->seeder->getUserGroupIdByName($group);

                if ($id === null) {
                    throw new \InvalidArgumentException(sprintf('Could not find a usergroup with name "%s"', $group));
                }

                return $id;
            };
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getGroup(): int
    {
        return $this->data['gebruikersgroep'] instanceof \Closure ? $this->data['gebruikersgroep']() : $this->data['gebruikersgroep'];
    }

    /**
     * @param string $module
     *
     * @return $this
     */
    public function setModule(string $module): self
    {
        $this->data['module'] = $module;

        return $this;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->data['module'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setCreate(int $permission): self
    {
        $this->data['toevoegen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreate(): int
    {
        return $this->data['toevoegen'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setRead(int $permission): self
    {
        $this->data['lezen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getRead(): int
    {
        return $this->data['lezen'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setUpdate(int $permission): self
    {
        $this->data['wijzigen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdate(): int
    {
        return $this->data['wijzigen'];
    }

    /**
     * @param int $permission
     *
     * @return $this
     */
    public function setDelete(int $permission): self
    {
        $this->data['verwijderen'] = $permission;

        return $this;
    }

    /**
     * @return int
     */
    public function getDelete(): int
    {
        return $this->data['verwijderen'];
    }
}
