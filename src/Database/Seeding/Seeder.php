<?php

namespace WebbeheerLaravel\Database\Seeding;

class Seeder
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Module[]
     */
    protected $modules = [];

    /**
     * @var \WebbeheerLaravel\Database\Seeding\ModuleCategory[]
     */
    protected $moduleCategories = [];

    /**
     * @var \WebbeheerLaravel\Database\Seeding\User[]
     */
    protected $users = [];

    /**
     * @var \WebbeheerLaravel\Database\Seeding\UserPermission[]
     */
    protected $userPermissions = [];

    /**
     * @var \WebbeheerLaravel\Database\Seeding\UserGroup[]
     */
    protected $userGroups = [];

    /**
     * @var \WebbeheerLaravel\Database\Seeding\UserGroupPermission[]
     */
    protected $userGroupPermissions = [];

    /**
     * @param string     $module
     * @param string     $title
     * @param int|string $moduleCategory Name or id
     *
     * @throws \InvalidArgumentException
     *
     * @return \WebbeheerLaravel\Database\Seeding\Module
     */
    public function addModule(string $module, string $title, $moduleCategory): Module
    {
        $this->modules[] = new Module($this, $module, $title, $moduleCategory);

        return end($this->modules);
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $icon
     *
     * @return \WebbeheerLaravel\Database\Seeding\ModuleCategory
     */
    public function addModuleCategory(
        string $title,
        string $description,
        string $icon = 'content'
    ): ModuleCategory {
        $this->moduleCategories[] = new ModuleCategory($this, $title, $description, $icon);

        return end($this->moduleCategories);
    }

    /**
     * @return \WebbeheerLaravel\Database\Seeding\User
     */
    public function addUser(): User
    {
        $this->users[] = new User($this);

        return end($this->users);
    }

    /**
     * @param string     $module
     * @param int|string $user   Username or id
     *
     * @throws \InvalidArgumentException
     *
     * @return \WebbeheerLaravel\Database\Seeding\UserPermission
     */
    public function addUserPermission(string $module, $user): UserPermission
    {
        $this->userPermissions[] = new UserPermission($this, $module, $user);

        return end($this->userPermissions);
    }

    /**
     * @param string $title
     *
     * @return \WebbeheerLaravel\Database\Seeding\UserGroup
     */
    public function addUserGroup(string $title): UserGroup
    {
        $this->userGroups[] = new UserGroup($this, $title);

        return end($this->userGroups);
    }

    /**
     * @param string     $module
     * @param int|string $group  Name or id
     *
     * @throws \InvalidArgumentException
     *
     * @return \WebbeheerLaravel\Database\Seeding\UserGroupPermission
     */
    public function addUserGroupPermission(string $module, $group): UserGroupPermission
    {
        $this->userGroupPermissions[] = new UserGroupPermission($this, $module, $group);

        return end($this->userGroupPermissions);
    }

    /**
     * @param string $username
     *
     * @return int|null
     */
    public function getUserIdByUsername(string $username)
    {
        return \DB::table('gebruikers')
            ->where('gebruikersnaam', '=', $username)
            ->pluck('id')
            ->first();
    }

    /**
     * @param string $name
     *
     * @return int|null
     */
    public function getUserGroupIdByName(string $name)
    {
        return \DB::table('gebruikersgroepen')
            ->where('titel', '=', $name)
            ->pluck('id')
            ->first();
    }

    /**
     * @param string $name
     *
     * @return int|null
     */
    public function getModuleCategoryIdByName(string $name)
    {
        return \DB::table('rubriek_modules')
            ->where('titel', '=', $name)
            ->pluck('id')
            ->first();
    }

    public function truncateAll()
    {
        \DB::table('gebruikers')->truncate();
        \DB::table('gebruikersrechten')->truncate();
        \DB::table('gebruikersgroepen')->truncate();
        \DB::table('gebruikersgroeprechten')->truncate();
        \DB::table('modules')->truncate();
        \DB::table('rubriek_modules')->truncate();
    }

    public function createAll()
    {
        foreach ($this->userGroups as $item) {
            $existing = \DB::table('gebruikersgroepen')
                ->where('titel', '=', $item->getTitle())
                ->first();

            if ($existing === null) {
                \DB::table('gebruikersgroepen')
                    ->insert($item->create());
            } else {
                \DB::table('gebruikersgroepen')
                    ->where('id', '=', $existing->id)
                    ->update($item->create());
            }
        }

        foreach ($this->users as $item) {
            $existing = \DB::table('gebruikers')
                ->where('gebruikersnaam', '=', $item->getUsername())
                ->first();

            if ($existing === null) {
                \DB::table('gebruikers')
                    ->insert($item->create());
            } /** @noinspection MissingOrEmptyGroupStatementInspection @noinspection PhpStatementHasEmptyBodyInspection */ else {
                // Do not update the user to, among other things, prevent resetting the password
            }
        }

        foreach ($this->moduleCategories as $item) {
            $existing = \DB::table('rubriek_modules')
                ->where('titel', '=', $item->getTitle())
                ->first();

            if ($existing === null) {
                \DB::table('rubriek_modules')
                    ->insert($item->create());
            } else {
                \DB::table('rubriek_modules')
                    ->where('id', '=', $existing->id)
                    ->update($item->create());
            }
        }

        foreach ($this->modules as $item) {
            $existing = \DB::table('modules')
                ->where('module', '=', $item->getModule())
                ->first();

            if ($existing === null) {
                \DB::table('modules')
                    ->insert($item->create());
            } else {
                \DB::table('modules')
                    ->where('id', '=', $existing->id)
                    ->update($item->create());
            }
        }

        foreach ($this->userGroupPermissions as $item) {
            $existing = \DB::table('gebruikersgroeprechten')
                ->where('gebruikersgroep', '=', $item->getGroup())
                ->where('module', '=', $item->getModule())
                ->first();

            if ($existing === null) {
                \DB::table('gebruikersgroeprechten')
                    ->insert($item->create());
            } else {
                \DB::table('gebruikersgroeprechten')
                    ->where('gebruikersgroep', '=', $existing->gebruikersgroep)
                    ->where('module', '=', $existing->module)
                    ->update($item->create());
            }
        }

        $this->propagateUserPermissions();

        foreach ($this->userPermissions as $item) {
            $existing = \DB::table('gebruikersrechten')
                ->where('gebruiker', '=', $item->getUser())
                ->where('module', '=', $item->getModule())
                ->first();

            if ($existing === null) {
                \DB::table('gebruikersrechten')
                    ->insert($item->create());
            } else {
                \DB::table('gebruikersrechten')
                    ->where('gebruiker', '=', $existing->gebruiker)
                    ->where('module', '=', $existing->module)
                    ->update($item->create());
            }
        }
    }

    /**
     * @throws \InvalidArgumentException
     */
    protected function propagateUserPermissions()
    {
        foreach ($this->userGroupPermissions as $permission) {
            $group = $permission->getGroup();

            foreach ($this->users as $user) {
                if ($user->getGroup() === $group) {
                    $this->addUserPermission($permission->getModule(), $user->getUsername())
                        ->setCreate($permission->getCreate())
                        ->setRead($permission->getRead())
                        ->setUpdate($permission->getUpdate())
                        ->setDelete($permission->getDelete());
                }
            }
        }
    }
}
