<?php

namespace WebbeheerLaravel\Database\Seeding;

class User
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param \WebbeheerLaravel\Database\Seeding\Seeder $seeder
     */
    public function __construct(Seeder $seeder)
    {
        $this->seeder = $seeder;

        $this->data = [
            'gebruiker'       => '1',
            'groep'           => '1',
            'gebruikersnaam'  => null,
            'gebruikersgroep' => null,
            'wachtwoord_hash' => '',
            'voorletters'     => null,
            'achternaam'      => null,
            'emailadres'      => null,
            'avatar'          => '',
            'geblokkeerd'     => false,
            'resetkey'        => '',
        ];
    }

    /**
     * @return array
     */
    public function create(): array
    {
        return array_map(
            function ($value) {
                return $value instanceof \Closure ? $value() : $value;
            },
            $this->data
        );
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->data['gebruikersnaam'] = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->data['gebruikersnaam'];
    }

    /**
     * @param int|string $group Id or name
     *
     * @throws \InvalidArgumentException
     *
     * @return $this
     */
    public function setGroup($group): self
    {
        if (is_numeric($group)) {
            $this->data['gebruikersgroep'] = $group;
        } else {
            $this->data['gebruikersgroep'] = function () use ($group) {
                $id = $this->seeder->getUserGroupIdByName($group);

                if ($id === null) {
                    throw new \InvalidArgumentException(sprintf('Could not find a usergroup with name "%s"', $group));
                }

                return $id;
            };
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getGroup(): int
    {
        return $this->data['gebruikersgroep'] instanceof \Closure ? $this->data['gebruikersgroep']() : $this->data['gebruikersgroep'];
    }

    /**
     * @param string $initials
     *
     * @return $this
     */
    public function setInitials(string $initials): self
    {
        $this->data['voorletters'] = $initials;

        return $this;
    }

    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->data['achternaam'] = $lastName;

        return $this;
    }

    /**
     * @param string $emailaddress
     *
     * @return $this
     */
    public function setEmailaddress(string $emailaddress): self
    {
        $this->data['emailadres'] = $emailaddress;

        return $this;
    }

    /**
     * @param bool $blocked
     *
     * @return $this
     */
    public function setBlocked(bool $blocked): self
    {
        $this->data['geblokkeerd'] = $blocked;

        return $this;
    }
}
