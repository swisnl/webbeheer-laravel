<?php

namespace WebbeheerLaravel\Database\Seeding;

class UserGroup
{
    /**
     * @var \WebbeheerLaravel\Database\Seeding\Seeder
     */
    protected $seeder;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param \WebbeheerLaravel\Database\Seeding\Seeder $seeder
     * @param string                                    $title
     */
    public function __construct(Seeder $seeder, string $title)
    {
        $this->seeder = $seeder;

        $this->data = [
            'gebruiker' => '1',
            'groep'     => '1',
            'titel'     => $title,
        ];
    }

    /**
     * @return array
     */
    public function create(): array
    {
        return array_map(
            function ($value) {
                return $value instanceof \Closure ? $value() : $value;
            },
            $this->data
        );
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->data['titel'] = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->data['titel'];
    }
}
