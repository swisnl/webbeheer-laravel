<?php

namespace WebbeheerLaravel\Providers;

use Illuminate\Support\ServiceProvider;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        view()->composer('webbeheer-laravel::layouts/*', config('webbeheer-laravel.headhtml_view_composer'));
        view()->composer(config('webbeheer-laravel.view_layouts_folder').'/*', config('webbeheer-laravel.headhtml_view_composer'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}