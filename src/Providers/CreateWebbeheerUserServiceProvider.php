<?php

namespace WebbeheerLaravel\Providers;

use Illuminate\Support\ServiceProvider;
use WebbeheerLaravel\Commands\CreateWebbeheerUserCommand;


class CreateWebbeheerUserServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local')) {
            $this->commands([CreateWebbeheerUserCommand::class]);
        }
    }
}