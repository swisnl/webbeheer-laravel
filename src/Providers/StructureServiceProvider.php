<?php

namespace WebbeheerLaravel\Providers;

use Gwnobots\LaravelHead\LaravelHeadFacade;
use Gwnobots\LaravelHead\LaravelHeadServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use WebbeheerLaravel\Commands\FindLayourZones;
use WebbeheerLaravel\Commands\FindLayoutsCommand;
use WebbeheerLaravel\Commands\FindPortletControllersCommand;
use WebbeheerLaravel\Contracts\NodeRoutesGeneratorContract;

class StructureServiceProvider extends ServiceProvider
{

    protected $commands = [
        FindPortletControllersCommand::class,
        FindLayoutsCommand::class,
        FindLayourZones::class,
    ];

    protected $providers = [
        LaravelHeadServiceProvider::class,
        ComposerServiceProvider::class,
        CreateWebbeheerUserServiceProvider::class,
    ];

    /**
     * Boot structure routing service and create routes to noded.
     *
     * @return void
     */
    public function boot()
    {

        if (!$this->app->routesAreCached() && Schema::hasTable('sm_nodes') ) {
            /** @var NodeRoutesGeneratorContract $nodeRoutesGenerator */
            $nodeRoutesGenerator = app()->make(config('webbeheer-laravel.node_routes_generator'));
            $nodeRoutesGenerator->registerNodesRoutes();
        }


        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'webbeheer-laravel');

        $this->publishes([
            __DIR__.'/../../resources/views' => base_path('resources/views/vendor/webbeheer-laravel'),
        ]);
        $this->publishes([
            __DIR__.'/../../config/webbeheer-laravel.php' => base_path('config/webbeheer-laravel.php'),
        ]);

        foreach($this->providers as $provider){
            $this->app->register($provider);
        }
    }

    /**
     * Load config.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/webbeheer-laravel.php',
            'webbeheer-laravel'
        );

        $this->commands($this->commands);

        AliasLoader::getInstance()->alias('Head', LaravelHeadFacade::class);

    }

}
