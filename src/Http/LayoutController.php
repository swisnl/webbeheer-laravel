<?php
namespace WebbeheerLaravel\Http;

use App\Http\Controllers\Controller;
use Illuminate\Routing\ControllerDispatcher;
use Illuminate\Routing\Router;
use Illuminate\Routing\Route;
use WebbeheerLaravel\Helpers\LayoutHelper;
use WebbeheerLaravel\Structure\Model\Content;
use WebbeheerLaravel\Structure\Model\Navigation;
use WebbeheerLaravel\Structure\Model\Node;

class LayoutController extends Controller
{
    /**
     * @return string
     */
    public function buildLayout(Router $router)
    {
        /**
         * @var $router Router
         */
//        $router = app('router');

        /**
         * @var $route Route
         */
        $route = $router->current();
        $nodeId = $route->getAction()['node_id'];

        /**
         * @var $node Node
         */
        $node = Node::find($nodeId);
        $layoutHelper = new LayoutHelper($node->layout);

        app()->instance(Node::class, $node);
        app()->instance(LayoutHelper::class, $layoutHelper);

        $autorender = Navigation::where('autorender', '=', 1)->get();
        foreach($autorender as $navigation){
            if(version_compare(app()->VERSION(), '5.1.0', '>=') && version_compare(app()->VERSION(), '5.3.0', '<')) {
                $response = (new ControllerDispatcher($router, app()))
                  ->dispatch(
                    $router->getCurrentRoute(),
                    $router->getCurrentRequest(),
                    config('webbeheer-laravel.navigation_controller_class'),
                    strtolower($navigation->name)
                  );

                if (!$response->isOk()) {
                    return $response;
                }
                $layoutHelper->addRawContent($navigation->name, $response->content());
            } else {
                $controllerClass = config('webbeheer-laravel.navigation_controller_class');
                $response = (new ControllerDispatcher(app()))
                  ->dispatch(
                    $router->getCurrentRoute(),
                    app()->make($controllerClass),
                    strtolower($navigation->name)
                  );
                $layoutHelper->addRawContent($navigation->name, $response);
            }
        }


        foreach ($node->visibleContents() as $content) {
            app()->instance(Content::class, $content);

            /** @var \WebbeheerLaravel\Structure\Resolvers\ContentControllerResolverInterface $contentControllerResolver */
            $contentControllerResolver = config('webbeheer-laravel.content_controller_resolver');
            if(version_compare(app()->VERSION(), '5.1.0', '>=') && version_compare(app()->VERSION(), '5.3.0', '<')) {
                $response = (new ControllerDispatcher($router, app()))
                  ->dispatch(
                    $router->getCurrentRoute(),
                    $router->getCurrentRequest(),
                    $contentControllerResolver::controllerName($content),
                    $contentControllerResolver::actionName($content)
                  );

                if (!$response->isOk()) {
                    return $response;
                }
                $responseContent = $response->content();
            } else {
                $response = (new ControllerDispatcher(app()))
                  ->dispatch(
                    $router->getCurrentRoute(),
                    app()->make($contentControllerResolver::controllerName($content)),
                    $contentControllerResolver::actionName($content)
                  );
                $responseContent = $response;
            }

            $layoutHelper->addContent(
              $content->block,
              $responseContent
            );

        }

        if (view()->exists('layouts.'.$node->layout->name)) {
            return view('layouts.'.$node->layout->name, $layoutHelper->blocks());
        }

        return view('webbeheer-laravel::layouts/'.$node->layout->name, $layoutHelper->blocks());
    }
}
