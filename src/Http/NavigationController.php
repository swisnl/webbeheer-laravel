<?php
namespace WebbeheerLaravel\Http;

use App\Http\Controllers\Controller;
use Lavary\Menu\Facade as Menu;
use WebbeheerLaravel\Helpers\Structure;
use WebbeheerLaravel\Structure\Model\Content;
use WebbeheerLaravel\Structure\Model\Node;
use WebbeheerLaravel\Structure\Navigation;

class NavigationController extends Controller
{

    public function mainnav(Node $node, Content $contents){


        $name = 'mainnav';
        $nav = new Navigation();
        $nav->setRootByName($name);
        $nav->setCurrentNode($node->id);
        $nodes = $nav->getNavigatie();

        return view('webbeheer-laravel::navigation/' . $name, ['nodes' => $nodes, 'homepageNodeId' => 3]);
    }

    public function subnav(Node $node, Content $contents){
        $name = 'subnav';
        $navigatie = new Navigation();
        $navigatie->setRootId($node->id);
        $navigatie->setCurrentNode($node->id);

        if(isset($contents->parameters->active_childnodes_only) && $contents->parameters->active_childnodes_only) {
            $navigatie->setRecursive(true);
            $navigatie->setGetActiveSubnodesOnly(true);
        }

        return view('webbeheer-laravel::navigation/' . $name, ['nodes' => $navigatie->getNavigatie(), 'homepageNodeId' => 3]);



    }

}