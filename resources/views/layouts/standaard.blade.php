<!DOCTYPE html>
<!--[if lte IE 8]> <html class="no-js ie8" lang="nl"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="nl"> <!--<![endif]-->
<head>
    {!! @$headHtml !!}
</head>
<body id="layout-standaard">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ config('ROOTURL') }}">{{ config('KLANTNAAM') }}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            {!! @$Mainnav !!}
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
            {!! @$leftColumn !!}
        </div>
        <div class="col-xs-12 col-md-6">
            {!! @$middleColumn !!}
        </div>
        <div class="col-xs-12 col-md-3">
            {!! @$rightColumn !!}
        </div>
    </div>
</div>


<footer id="footer" class="container">
    <div class="row">
        <div class="col-xs-12">
            {!! @$Footernav !!}
        </div>
    </div>

</footer>

{!! @$headHtmlBottom !!}
</body>
</html>