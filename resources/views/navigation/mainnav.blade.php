<ul class="nav navbar-nav">
    @foreach($nodes as $node)
        @if(!$node['hidden'])
            <li @if($node['active']) class="active"@endif><a href="@if($homepageNodeId == $node['id']){{ url('/') }} @else{!! $node['link'] !!}@endif" title="{!! $node['title'] !!}">{!! $node['title'] !!}</a></li>
        @endif
    @endforeach
</ul>