@if(count($nodes) > 0)
    <div class="sub-nav">
        <ul class="unstyled">
@endif

@foreach($nodes as $node)
        <li @if(isset($node['childnodes']) && $node['active'] == 1) class="has-active-sub-nav" @endif>
            <a href="{!! $node['link'] !!}" title="{{ $node['title'] }}" @if( $node['active']) class="active" @endif>{!! $node['title'] !!}</a>

            @if(isset($node['childnodes']))
            <ul class="unstyled">
                @foreach($node['childnodes'] as $child)
                <li @if( $child == end($node['childnodes'])) class="last" @endif ><a href="{{ $child['link'] }}" title="{{ $child['title'] }}" @if($child['active']) class="active" @endif>{{ $child['title'] }}</a></li>
                @endforeach
            </ul>
            @endif
        </li>
@endforeach

@if(count($nodes) > 0)
        </ul>
    </div>
@endif
